from pykrx import stock

kospi = []
kosdaq = []
etf = []
etn = []
dict = {}
for ticker in stock.get_etf_ticker_list():
    name = stock.get_etf_ticker_name(ticker)
    etf.append(ticker)
    dict[ticker] = {'name': name, 'type': 'etf'}
for ticker in stock.get_etn_ticker_list():
    name = stock.get_etf_ticker_name(ticker)
    etn.append(ticker)
    dict[ticker] = {'name': name, 'type': 'etn'}
for ticker in stock.get_market_ticker_list(market='KOSPI'):
    name = stock.get_market_ticker_name(ticker)
    kospi.append(ticker)
    dict[ticker] = {'name': name, 'type': 'kospi'}
for ticker in stock.get_market_ticker_list(market='KOSDAQ'):
    name = stock.get_market_ticker_name(ticker)
    kosdaq.append(ticker)
    dict[ticker] = {'name': name, 'type': 'kosdaq'}
with open('ticker_name_dict_3.py', 'w') as f:
    print('dict = {', file=f)
    for k in dict:
        print(f"    '{k}': {dict[k]},", file=f)
    print('}', file=f)
    print(file=f)
    print('kospi = [', file=f)
    for k in kospi:
        print(f"    '{k}',", file=f)
    print(']', file=f)
    print(file=f)
    print('kosdaq = [', file=f)
    for k in kosdaq:
        print(f"    '{k}',", file=f)
    print(']', file=f)
    print(file=f)
    print('etf = [', file=f)
    for k in etf:
        print(f"    '{k}',", file=f)
    print(']', file=f)
    print(file=f)
    print('etn = [', file=f)
    for k in etn:
        print(f"    '{k}',", file=f)
    print(']', file=f)
    print(file=f)
    print('kospi_200 = [', file=f)
    for k in stock.get_index_portfolio_deposit_file('1028'):
        print(f"    '{k}',", file=f)
    print(']', file=f)
    print(file=f)
    print(file=f)
    print('def find(ticker):', file=f)
    print('    try:', file=f)
    print('        return dict[ticker]', file=f)
    print('    except:', file=f)
    print("        return {'name': f'{ticker}', 'type': 'unknown'}", file=f)
