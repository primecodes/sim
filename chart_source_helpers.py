from datetime import datetime
from pathlib import Path

import pandas as pd
import pybithumb
import pytz
import pyupbit
import yfinance as yf
from pykrx import stock

import ticker_name_dict_3

KST = pytz.timezone("Asia/Seoul")


def cache_folder():
    return Path('..') / 'cache'


def cache_folder_2():
    return Path('/') / 'workspace' / 'stock' / 'data'


def add_arguments(parser):
    parser.add_argument(
        'ticker',
        help='symbol name',
    )
    parser.add_argument(
        '-s',
        '--source',
        default='bithumb',
        help='chart data source',
    )
    parser.add_argument(
        '-i',
        '--interval',
        default=None,
        help='chart time frame interval, daily if None',
    )
    parser.add_argument(
        '-r',
        '--reload',
        action='store_true',
        help='reload data from chart source',
    )


def load(args):
    source = args.source
    if source == 'bithumb':
        return load_bithumb_args(args)
    if source == 'upbit':
        return load_upbit_args(args)
    if source == 'krx':
        return load_krx_args(args)
    if source == 'yf':
        return load_yf_args(args)
    if source == 'cache':
        return load_cache_args(args)
    raise Exception(f'Unknown source: {source}')


def load_bithumb_args(args):
    ticker = args.ticker
    interval = args.interval
    return load_bithumb(ticker, interval=interval)


def load_upbit_args(args):
    ticker = args.ticker
    interval = args.interval
    reload = args.reload
    return load_upbit(ticker, interval=interval, reload=reload)


def load_yf_args(args):
    symbol = args.ticker
    interval = args.interval
    return load_yf(symbol, interval)


def load_krx_args(args):
    ticker = args.ticker
    interval = args.interval
    reload = args.reload
    return load_krx(ticker, interval=interval, reload=reload)


def load_cache_args(args):
    ticker = args.ticker
    interval = args.interval
    return load_cache(ticker, interval=interval)


def load_bithumb(ticker, interval='24h'):
    if interval is None:
        interval = '24h'
    df = pybithumb.get_candlestick(ticker, chart_intervals=interval)
    df = df.reset_index()
    df = df.rename(
        columns={
            'time': 'Time',
            'open': 'Open',
            'high': 'High',
            'low': 'Low',
            'close': 'Close',
            'volume': 'Volume',
        })
    df = df.set_index('Time')
    return df, interval


def load_upbit(ticker, interval='day', reload=False):
    if interval is None:
        interval = 'day'
    if interval not in [
            'day',
            'minute1',
            'minute3',
            'minute5',
            'minute10',
            'minute15',
            'minute30',
            'minute60',
            'minute240',
            'week',
            'month',
    ]:
        raise Exception(f'invalid interval: {interval}')
    fname = cache_folder() / 'upbit' / interval / f'{ticker}.csv'
    if not reload and fname.is_file():
        df_cached = pd.read_csv(fname)
        df_cached['Time'] = pd.to_datetime(df_cached['Time'])
        return df_cached.set_index('Time'), interval
    if fname.is_file():
        df_cached = pd.read_csv(fname)
        df_cached['Time'] = pd.to_datetime(df_cached['Time'])
        fromDateTime = df_cached['Time'].max()
    else:
        fromDateTime = None
    df = pyupbit.get_ohlcv_from(ticker,
                                fromDatetime=fromDateTime,
                                interval=interval)
    if df is not None:
        df = df.reset_index().rename(
            columns={
                'index': 'Time',
                'open': 'Open',
                'high': 'High',
                'low': 'Low',
                'close': 'Close',
                'volume': 'Volume',
                'value': 'Value',
            })
    if fromDateTime is not None:
        df = pd.concat([df_cached[:-1], df])
    df = df.set_index('Time')
    fname.parent.mkdir(exist_ok=True, parents=True)
    df.to_csv(fname)
    return df, interval


def load_yf(symbol, interval='1d'):
    if interval is None:
        interval = '1d'
    if interval not in [
            '1m',
            '2m',
            '5m',
            '15m',
            '30m',
            '60m',
            '90m',
            '1h',
            '1d',
            '5d',
            '1wk',
            '1mo',
            '3mo',
    ]:
        raise Exception(f'invalid interval: {interval}')
    ticker = yf.Ticker(symbol)
    ticker.history()
    df = ticker.history(period='max', interval=interval)
    df = df.reset_index()
    if interval.endswith('m') or interval.endswith('h'):
        df = df.rename(columns={'Datetime': 'Time'})
    else:
        df = df.rename(columns={'Date': 'Time'})
    df = df.set_index('Time')
    return df, interval


def load_krx(ticker, interval='d', reload=False):
    if interval is None:
        interval = 'd'
    if interval not in ['d', 'm', 'y']:
        raise Exception(f'invalid interval: {interval}')
    todate = datetime.now().strftime('%Y%m%d')
    fname = cache_folder() / 'krx' / interval / f'{ticker}.csv'
    if not reload and fname.is_file():
        df1 = pd.read_csv(fname)
        df1['Time'] = pd.to_datetime(df1['Time'])
        return df1.set_index('Time'), interval
    if fname.is_file():
        df1 = pd.read_csv(fname)
        df1['Time'] = pd.to_datetime(df1['Time'])
        fromdate = df1['Time'].max().strftime('%Y%m%d')
        df1 = df1[:-1]
        df2 = load_krx_by_date(ticker, fromdate, todate, interval)
        df2['Time'] = pd.to_datetime(
            df2['Time']).dt.tz_localize('UTC').dt.tz_convert(KST)
        df = pd.concat([df1, df2]).set_index('Time')
        df.to_csv(fname)
        return df, interval
    else:
        fromdate = '19800101'
        df = load_krx_by_date(ticker, fromdate, todate, interval)
        df['Time'] = pd.to_datetime(
            df['Time']).dt.tz_localize('UTC').dt.tz_convert(KST)
        df = df.set_index('Time')
        fname.parent.mkdir(exist_ok=True, parents=True)
        df.to_csv(fname)
        return df, interval


def load_krx_by_date(ticker, fromdate, todate, freq):
    r = ticker_name_dict_3.find(ticker)
    print(r)
    type = r['type']
    if type == 'etf':
        df = stock.get_etf_ohlcv_by_date(fromdate, todate, ticker, freq=freq)
    elif type == 'etn':
        raise Exception(f'etn data not supported')
    else:
        df = stock.get_market_ohlcv_by_date(fromdate,
                                            todate,
                                            ticker,
                                            adjusted=True)
    return df.reset_index().rename(
        columns={
            '날짜': 'Time',
            '시가': 'Open',
            '고가': 'High',
            '저가': 'Low',
            '종가': 'Close',
            '거래량': 'Volume',
        })


def load_cache(ticker, interval='day'):
    fname = cache_folder_2() / interval / f'{ticker}.csv'
    df = pd.read_csv(fname)
    df['Time'] = pd.to_datetime(
        df['Time'], unit='s').dt.tz_localize('UTC').dt.tz_convert(KST)
    df = df.set_index('Time')
    return df, interval


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    print(args)
    df = load(args)
    print(df)