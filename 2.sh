freq=$1
dir=temp
mkdir -p $dir/err
for t in `cat upbit_tickers.txt`; do
  echo $t
  fname=$t,$freq,12m
  python sim.py -s cache -i upbit/$freq $t > $dir/$fname.txt 2> $dir/err/$fname.err
done