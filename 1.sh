freq=$1
dir=temp
mkdir -p $dir/err
for t in `cat upbit_tickers.txt`; do
  for s in 0 1 2 3 4 5 6 7 8 9 10 11 12; do
    echo $t $s
    fname=$t,$freq.1m-$s
    python sim.py -s cache -i upbit/$freq $t -p 1 --shift=$s > $dir/$fname.txt 2> $dir/err/$fname.err
  done
done
