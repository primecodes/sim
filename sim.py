import argparse
from datetime import datetime
from pathlib import Path

import pandas as pd
from dateutil.relativedelta import relativedelta

import chart_source_helpers
import mpl_helpers
import raposa


def main():

    def argparser():
        parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        chart_source_helpers.add_arguments(parser)
        parser.add_argument(
            '--shift',
            type=int,
            default=0,
            help='shift time period in months',
        )
        parser.add_argument(
            '-p',
            '--period',
            type=int,
            default=12,
            help='time period to invest in months',
        )
        return parser

    strats = {
        'RSIReversionStrategy': raposa.RSIReversionStrategy,
        'RSI2Strategy': raposa.RSI2Strategy,
        'RSIMomentumStrategy': raposa.RSIMomentumStrategy,
        'RSITrendStrategy': raposa.RSITrendStrategy,
        'midDonCrossOver': raposa.midDonCrossOver,
        'donChannelBreakout': raposa.donChannelBreakout,
        'donReversal': raposa.donReversal,
        'PSARTrendStrategy': raposa.PSARTrendStrategy,
        'RSITrendPSARStrategy': raposa.RSITrendPSARStrategy,
        'TrendIntensityPSARStrategy': raposa.TrendIntensityPSARStrategy,
        'BBMeanReversion': raposa.BBMeanReversion,
        'BBBreakout': raposa.BBBreakout,
        'DoubleBBBreakout': raposa.DoubleBBBreakout,
        'BBWidthEMACross': raposa.BBWidthEMACross,
        'StochRSIReversionStrategy': raposa.StochRSIReversionStrategy,
        'StochRSIMomentumStrategy': raposa.StochRSIMomentumStrategy,
        'MACDReversionStrategy': raposa.MACDReversionStrategy,
        'MACDSignalStrategy': raposa.MACDSignalStrategy,
        'MACDMomentumStrategy': raposa.MACDMomentumStrategy,
        'MACDSignalMomentumStrategy': raposa.MACDSignalMomentumStrategy,
        'RSIDivergenceStrategy': raposa.RSIDivergenceStrategy,
        'RSIDivergenceWithTrendStrategy':
        raposa.RSIDivergenceWithTrendStrategy,
        'SMAMeanReversion': raposa.SMAMeanReversion,
        'SMAMeanReversionSafety': raposa.SMAMeanReversionSafety,
        'MADStrategy': raposa.MADStrategy,
        'MADSigmaStrategy': raposa.MADSigmaStrategy,
    }
    args = argparser().parse_args()
    print(args)
    ticker = args.ticker
    period = args.period
    source = args.source
    shift = args.shift
    to_d = datetime.now() + relativedelta(days=1) - relativedelta(months=shift)
    from_d = to_d - relativedelta(months=period)
    data, interval = chart_source_helpers.load(args)
    data = data[(data.index >= from_d.strftime('%Y-%m-%d'))
                & (data.index < to_d.strftime('%Y-%m-%d'))]
    begin = data.index[0].strftime('%Y-%m-%d')
    end = data.index[-1].strftime('%Y-%m-%d')
    print(ticker, begin, end)
    stats = []
    root = Path('temp') / f'{ticker},{interval},{source},{begin},{end}'
    root.mkdir(parents=True, exist_ok=True)
    mpl_helpers.plot(data, f'{ticker},{interval},{source}',
                     root / f'prices.svg')
    for name in strats:
        strat = strats[name]
        df = strat(data.copy())
        stat_df = raposa.getStratStats(df['strat_log_returns'])
        mpl_helpers.plot_2(df, f'{ticker},{interval},{source}', name,
                           root / 'strategies' / f'{name}.svg')
        stats.append(pd.DataFrame(stat_df, index=[name]))
    stats.append(
        pd.DataFrame(raposa.getStratStats(df['log_returns']),
                     index=['Buy and Hold']))
    stats_df = pd.concat(stats).sort_values(by='tot_returns')
    with pd.option_context('display.width', None):
        print(stats_df)
    fname = root / 'stats.csv'
    stats_df.to_csv(fname)
    root2 = root / 'strategies'
    root2.mkdir(parents=True, exist_ok=True)
    for index, row in stats_df.iterrows():
        with open(root2 / f'{index}.html', 'w') as f:
            print('<div align="center">', file=f)
            print(f'<img src="{index}.svg" />', file=f)
            print('</div>', file=f)
            print('<table align="center">', file=f)
            print('<tr>', file=f)
            print('<th align="right">Total Returns</th>', file=f)
            print('<th align="right">Annual Returns</th>', file=f)
            print('<th align="right">Annual Volatility</th>', file=f)
            print('<th align="right">Sortino Ratio</th>', file=f)
            print('<th align="right">Sharpe Ratio</th>', file=f)
            print('<th align="right">Max Drawdown</th>', file=f)
            print('<th align="right">Longest Drawdown</th>', file=f)
            print('</tr>', file=f)
            print('<tr>', file=f)
            print(f'<td>{row["tot_returns"]}</td>', file=f)
            print(f'<td>{row["annual_returns"]}</td>', file=f)
            print(f'<td>{row["annual_volatility"]}</td>', file=f)
            print(f'<td>{row["sortino_ratio"]}</td>', file=f)
            print(f'<td>{row["sharpe_ratio"]}</td>', file=f)
            print(f'<td>{row["max_drawdown"]}</td>', file=f)
            print(f'<td>{row["longest_drawdown"]}</td>', file=f)
            print('</tr>', file=f)
            print('</table>', file=f)
            print('<br />', file=f)
            print('<br />', file=f)
            # print('<a href="..">[Back]</a>', file=f)
            print('<table align="center">', file=f)
            print('<tr>', file=f)
            print('<th></th>', file=f)
            print('<th>Total Returns</th>', file=f)
            print('<th>Annual Returns</th>', file=f)
            print('<th>Annual Volatility</th>', file=f)
            print('<th>Sortino Ratio</th>', file=f)
            print('<th>Sharpe Ratio</th>', file=f)
            print('<th>Max Drawdown</th>', file=f)
            print('<th>Longest Drawdown</th>', file=f)
            for index_, row in stats_df.iterrows():
                if index_ == 'Buy and Hold':
                    print('<tr bgcolor="yellow">', file=f)
                    print(f'<td>{index_}</td>', file=f)
                elif index_ == index:
                    print('<tr bgcolor="#eee">', file=f)
                    print(f'<td>{index_}</td>', file=f)
                else:
                    print('<tr>', file=f)
                    print(f'<td><a href="{index_}.html">{index_}</a></td>',
                          file=f)
                print(f'<td>{row["tot_returns"]}</td>', file=f)
                print(f'<td>{row["annual_returns"]}</td>', file=f)
                print(f'<td>{row["annual_volatility"]}</td>', file=f)
                print(f'<td>{row["sortino_ratio"]}</td>', file=f)
                print(f'<td>{row["sharpe_ratio"]}</td>', file=f)
                print(f'<td>{row["max_drawdown"]}</td>', file=f)
                print(f'<td>{row["longest_drawdown"]}</td>', file=f)
                print('</tr>', file=f)
            print('</table>', file=f)
    with open(root / 'index.html', 'w') as f:
        print(
            f'<h1 align="center">{ticker},{interval},{source},{begin},{end}</h1>',
            file=f)
        print('<div align="center">', file=f)
        print('<img src="prices.svg" />', file=f)
        print('</div>', file=f)
        print('<table align="center">', file=f)
        print('<tr>', file=f)
        print('<th></th>', file=f)
        print('<th>Total Returns</th>', file=f)
        print('<th>Annual Returns</th>', file=f)
        print('<th>Annual Volatility</th>', file=f)
        print('<th>Sortino Ratio</th>', file=f)
        print('<th>Sharpe Ratio</th>', file=f)
        print('<th>Max Drawdown</th>', file=f)
        print('<th>Longest Drawdown</th>', file=f)
        for index, row in stats_df.iterrows():
            if index == 'Buy and Hold':
                print('<tr bgcolor="yellow">', file=f)
                print(f'<td>{index}</td>', file=f)
            else:
                print('<tr>', file=f)
                print(
                    f'<td><a href="strategies/{index}.html">{index}</a></td>',
                    file=f)
            print(f'<td>{row["tot_returns"]}</td>', file=f)
            print(f'<td>{row["annual_returns"]}</td>', file=f)
            print(f'<td>{row["annual_volatility"]}</td>', file=f)
            print(f'<td>{row["sortino_ratio"]}</td>', file=f)
            print(f'<td>{row["sharpe_ratio"]}</td>', file=f)
            print(f'<td>{row["max_drawdown"]}</td>', file=f)
            print(f'<td>{row["longest_drawdown"]}</td>', file=f)
            print('</tr>', file=f)
        print('</table>', file=f)


main()