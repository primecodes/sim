import argparse

import pyupbit


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fiat', default='')
    return parser


args = argparser().parse_args()
for ticker in sorted(pyupbit.get_tickers(fiat=args.fiat)):
    print(ticker)
