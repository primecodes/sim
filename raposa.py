from collections import deque

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema

SHORTS = False


def calcReturns(df):
    df["returns"] = df["Close"] / df["Close"].shift(1)
    df["log_returns"] = np.log(df["returns"])
    df["cum_returns"] = np.exp(df["log_returns"].cumsum()) - 1
    df["peak"] = df["cum_returns"].cummax()
    df["strat_returns"] = df["position"].shift(1) * df["returns"]
    df["strat_log_returns"] = df["position"].shift(1) * df["log_returns"]
    df["strat_cum_returns"] = np.exp(df["strat_log_returns"].cumsum()) - 1
    df["strat_peak"] = df["strat_cum_returns"].cummax()
    return df


def getStratStats(log_returns: pd.Series, risk_free_rate: float = 0.02):
    stats = {}
    stats["tot_returns"] = np.exp(log_returns.sum()) - 1
    stats["annual_returns"] = np.exp(log_returns.mean() * 252) - 1
    stats["annual_volatility"] = log_returns.std() * np.sqrt(252)
    annualized_downside = log_returns.loc[log_returns < 0].std() * np.sqrt(252)
    stats["sortino_ratio"] = (
        stats["annual_returns"] - risk_free_rate
    ) / annualized_downside
    if stats["annual_volatility"] == 0:
        stats["sharpe_ratio"] = 0
    else:
        stats["sharpe_ratio"] = (stats["annual_returns"] - risk_free_rate) / stats[
            "annual_volatility"
        ]
    cum_returns = log_returns.cumsum() - 1
    peak = cum_returns.cummax()
    drawdown = peak - cum_returns
    max_idx = drawdown.argmax()
    stats["max_drawdown"] = 1 - np.exp(cum_returns.iloc[max_idx]) / np.exp(
        peak.iloc[max_idx]
    )
    strat_dd = drawdown[drawdown == 0]
    strat_dd_diff = strat_dd.index[1:] - strat_dd.index[:-1]
    strat_dd_days = strat_dd_diff.map(lambda x: x.days).values
    try:
        strat_dd_days = np.hstack(
            [strat_dd_days, (drawdown.index[-1] - strat_dd.index[-1]).days]
        )
        stats["longest_drawdown"] = strat_dd_days.max()
    except:
        stats["longest_drawdown"] = 0
    return {k: np.round(v, 4) if type(v) == np.float64 else v for k, v in stats.items()}


def hurst(price, min_lag=2, max_lag=100):
    lags = np.arange(min_lag, max_lag + 1)
    tau = [np.std(np.subtract(price[lag:], price[:-lag])) for lag in lags]
    m = np.polyfit(np.log10(lags), np.log10(tau), 1)
    return m, lags, tau


def calcMAD(price: pd.Series, fast_ma: int = 21, slow_ma: int = 200) -> pd.Series:
    return price.rolling(fast_ma).mean() / price.rolling(slow_ma).mean()


def _calcEMA(P, last_ema, N):
    return (P - last_ema) * (2 / (N + 1)) + last_ema


def calcEMA(data: pd.DataFrame, N: int, key: str = "Close"):
    # Initialize series
    data["SMA_" + str(N)] = data[key].rolling(N).mean()
    ema = np.zeros(len(data)) + np.nan
    for i, _row in enumerate(data.iterrows()):
        row = _row[1]
        if np.isnan(ema[i - 1]):
            ema[i] = row["SMA_" + str(N)]
        else:
            ema[i] = _calcEMA(row[key], ema[i - 1], N)
    data["EMA_" + str(N)] = ema.copy()
    return data


def getHigherLows(data: np.array, order=5, K=2):
    """
    Finds consecutive higher lows in price pattern.
    Must not be exceeded within the number of periods indicated by the width
    parameter for the value to be confirmed.
    K determines how many consecutive lows need to be higher.
    """
    # Get lows
    low_idx = argrelextrema(data, np.less, order=order)[0]
    lows = data[low_idx]
    # Ensure consecutive lows are higher than previous lows
    extrema = []
    ex_deque = deque(maxlen=K)
    for i, idx in enumerate(low_idx):
        if i == 0:
            ex_deque.append(idx)
            continue
        if lows[i] < lows[i - 1]:
            ex_deque.clear()

        ex_deque.append(idx)
        if len(ex_deque) == K:
            extrema.append(ex_deque.copy())

    return extrema


def getLowerHighs(data: np.array, order=5, K=2):
    """
    Finds consecutive lower highs in price pattern.
    Must not be exceeded within the number of periods indicated by the width
    parameter for the value to be confirmed.
    K determines how many consecutive highs need to be lower.
    """
    # Get highs
    high_idx = argrelextrema(data, np.greater, order=order)[0]
    highs = data[high_idx]
    # Ensure consecutive highs are lower than previous highs
    extrema = []
    ex_deque = deque(maxlen=K)
    for i, idx in enumerate(high_idx):
        if i == 0:
            ex_deque.append(idx)
            continue
        if highs[i] > highs[i - 1]:
            ex_deque.clear()

        ex_deque.append(idx)
        if len(ex_deque) == K:
            extrema.append(ex_deque.copy())

    return extrema


def getHigherHighs(data: np.array, order=5, K=2):
    """
    Finds consecutive higher highs in price pattern.
    Must not be exceeded within the number of periods indicated by the width
    parameter for the value to be confirmed.
    K determines how many consecutive highs need to be higher.
    """
    # Get highs
    high_idx = argrelextrema(data, np.greater, order=5)[0]
    highs = data[high_idx]
    # Ensure consecutive highs are higher than previous highs
    extrema = []
    ex_deque = deque(maxlen=K)
    for i, idx in enumerate(high_idx):
        if i == 0:
            ex_deque.append(idx)
            continue
        if highs[i] < highs[i - 1]:
            ex_deque.clear()

        ex_deque.append(idx)
        if len(ex_deque) == K:
            extrema.append(ex_deque.copy())

    return extrema


def getLowerLows(data: np.array, order=5, K=2):
    """
    Finds consecutive lower lows in price pattern.
    Must not be exceeded within the number of periods indicated by the width
    parameter for the value to be confirmed.
    K determines how many consecutive lows need to be lower.
    """
    # Get lows
    low_idx = argrelextrema(data, np.less, order=order)[0]
    lows = data[low_idx]
    # Ensure consecutive lows are lower than previous lows
    extrema = []
    ex_deque = deque(maxlen=K)
    for i, idx in enumerate(low_idx):
        if i == 0:
            ex_deque.append(idx)
            continue
        if lows[i] > lows[i - 1]:
            ex_deque.clear()

        ex_deque.append(idx)
        if len(ex_deque) == K:
            extrema.append(ex_deque.copy())

    return extrema


def getHHIndex(data: np.array, order=5, K=2):
    extrema = getHigherHighs(data, order, K)
    idx = np.array([i[-1] + order for i in extrema])
    return idx[np.where(idx < len(data))]


def getLHIndex(data: np.array, order=5, K=2):
    extrema = getLowerHighs(data, order, K)
    idx = np.array([i[-1] + order for i in extrema])
    return idx[np.where(idx < len(data))]


def getLLIndex(data: np.array, order=5, K=2):
    extrema = getLowerLows(data, order, K)
    idx = np.array([i[-1] + order for i in extrema])
    return idx[np.where(idx < len(data))]


def getHLIndex(data: np.array, order=5, K=2):
    extrema = getHigherLows(data, order, K)
    idx = np.array([i[-1] + order for i in extrema])
    return idx[np.where(idx < len(data))]


def getPeaks(data, key="Close", order=5, K=2):
    vals = data[key].values
    hh_idx = getHHIndex(vals, order, K)
    lh_idx = getLHIndex(vals, order, K)
    ll_idx = getLLIndex(vals, order, K)
    hl_idx = getHLIndex(vals, order, K)

    data[f"{key}_highs"] = np.nan
    data[f'{key}_highs'].iloc[hh_idx] = 1
    data[f'{key}_highs'].iloc[lh_idx] = -1
    data[f"{key}_highs"] = data[f"{key}_highs"].ffill().fillna(0)
    data[f"{key}_lows"] = np.nan
    data[f'{key}_lows'].iloc[ll_idx] = 1
    data[f'{key}_lows'].iloc[hl_idx] = -1
    data[f"{key}_lows"] = data[f"{key}_highs"].ffill().fillna(0)
    return data


def calcRSI(data, P=14):
    data["diff_close"] = data["Close"] - data["Close"].shift(1)
    data["gain"] = np.where(data["diff_close"] > 0, data["diff_close"], 0)
    data["loss"] = np.where(data["diff_close"] < 0, np.abs(data["diff_close"]), 0)
    data[["init_avg_gain", "init_avg_loss"]] = data[["gain", "loss"]].rolling(P).mean()
    avg_gain = np.zeros(len(data))
    avg_loss = np.zeros(len(data))

    for i, _row in enumerate(data.iterrows()):
        row = _row[1]
        if i < P - 1:
            last_row = row.copy()
            continue
        elif i == P - 1:
            avg_gain[i] += row["init_avg_gain"]
            avg_loss[i] += row["init_avg_loss"]
        else:
            avg_gain[i] += ((P - 1) * avg_gain[i - 1] + row["gain"]) / P
            avg_loss[i] += ((P - 1) * avg_loss[i - 1] + row["loss"]) / P
        last_row = row.copy()
    data["avg_gain"] = avg_gain
    data["avg_loss"] = avg_loss
    data["RS"] = data["avg_gain"] / data["avg_loss"]
    data["RSI"] = 100 - 100 / (1 + data["RS"])
    return data


def calcDonchianChannels(data: pd.DataFrame, period: int):
    data["upperDon"] = data["High"].rolling(period).max()
    data["lowerDon"] = data["Low"].rolling(period).min()
    data["midDon"] = (data["upperDon"] + data["lowerDon"]) / 2
    return data


def calcTrendIntensityIndex(data, P):
    data[f"SMA_{P}"] = data["Close"].rolling(P).mean()
    diff = data["Close"] - data[f"SMA_{P}"]
    pos_count = diff.map(lambda x: 1 if x > 0 else 0).rolling(int(P / 2)).sum()
    data["TII"] = 200 * pos_count / P
    return data


def calcBollingerBand(data, periods=20, m=2, label=None):
    """Calculates Bollinger Bands"""
    keys = ["UBB", "LBB", "TP", "STD", "TP_SMA"]
    if label is None:
        ubb, lbb, tp, std, tp_sma = keys
    else:
        ubb, lbb, tp, std, tp_sma = [i + "_" + label for i in keys]
    data[tp] = data.apply(lambda x: np.mean(x[["High", "Low", "Close"]]), axis=1)
    data[tp_sma] = data[tp].rolling(periods).mean()
    data[std] = data[tp].rolling(periods).std(ddof=1)
    data[ubb] = data[tp_sma] + m * data[std]
    data[lbb] = data[tp_sma] - m * data[std]

    return data


def calcStochOscillator(data, N=14):
    data["low_N"] = data["RSI"].rolling(N).min()
    data["high_N"] = data["RSI"].rolling(N).max()
    data["StochRSI"] = (
        100 * (data["RSI"] - data["low_N"]) / (data["high_N"] - data["low_N"])
    )
    return data


def calcStochRSI(data, P=14, N=14):
    data = calcRSI(data, P)
    data = calcStochOscillator(data, N)
    return data


def calcMACD(data: pd.DataFrame, N_fast: int, N_slow: int):
    assert N_fast < N_slow, "Fast EMA must be less than slow EMA parameter."
    # Add short term EMA
    data = calcEMA(data, N_fast)
    # Add long term EMA
    data = calcEMA(data, N_slow)
    # Subtract values to get MACD
    data["MACD"] = data[f"EMA_{N_fast}"] - data[f"EMA_{N_slow}"]
    return data


def calcMACD_(
    data: pd.DataFrame,
    N_fast: int,
    N_slow: int,
    signal_line: bool = True,
    N_sl: int = 9,
):
    assert N_fast < N_slow, "Fast EMA must be less than slow EMA parameter."
    # Add short term EMA
    data = calcEMA(data, N_fast)
    # Add long term EMA
    data = calcEMA(data, N_slow)
    # Subtract values to get MACD
    data["MACD"] = data[f"EMA_{N_fast}"] - data[f"EMA_{N_slow}"]
    if signal_line:
        data = calcEMA(data, N_sl, key="MACD")
        # Rename columns
        data.rename(
            columns={
                f"SMA_{N_sl}": f"SMA_MACD_{N_sl}",
                f"EMA_{N_sl}": f"SignalLine_{N_sl}",
            },
            inplace=True,
        )

    return data


def calcMACDSignal(data: pd.DataFrame, N_fast: int, N_slow: int, N_sl: int = 9):
    data = calcMACD(data, N_fast=N_fast, N_slow=N_slow)
    data = calcEMA(data, N_sl, key="MACD")
    # Rename columns
    data.rename(
        columns={
            f"SMA_{N_sl}": f"SMA_MACD_{N_sl}",
            f"EMA_{N_sl}": f"SignalLine_{N_sl}",
        },
        inplace=True,
    )
    return data


def calcMACDBars(data: pd.DataFrame, N_fast: int, N_slow: int, N_sl: int = 9):
    data = calcMACDSignal(data, N_fast=N_fast, N_slow=N_slow, N_sl=N_sl)
    data["MACD_bars"] = data["MACD"] - data[f"SignalLine_{N_sl}"]
    return data


def calcHullMA(price: pd.Series, N=50):
    SMA1 = price.rolling(N).mean()
    SMA2 = price.rolling(int(N / 2)).mean()
    return (2 * SMA2 - SMA1).rolling(int(np.sqrt(N))).mean()


def calcRMSE(price, indicator):
    sq_error = np.power(indicator - price, 2).sum()
    n = len(indicator.dropna())
    return np.sqrt(sq_error / n)


class PSAR:

    def __init__(self, init_af=0.02, max_af=0.2, af_step=0.02):
        self.max_af = max_af
        self.init_af = init_af
        self.af = init_af
        self.af_step = af_step
        self.extreme_point = None
        self.high_price_trend = []
        self.low_price_trend = []
        self.high_price_window = deque(maxlen=2)
        self.low_price_window = deque(maxlen=2)

        # Lists to track results
        self.psar_list = []
        self.af_list = []
        self.ep_list = []
        self.high_list = []
        self.low_list = []
        self.trend_list = []
        self._num_days = 0

    def calcPSAR(self, high, low):
        if self._num_days >= 3:
            psar = self._calcPSAR()
        else:
            psar = self._initPSARVals(high, low)

        psar = self._updateCurrentVals(psar, high, low)
        self._num_days += 1

        return psar

    def _initPSARVals(self, high, low):
        if len(self.low_price_window) <= 1:
            self.trend = None
            self.extreme_point = high
            return None

        if self.high_price_window[0] < self.high_price_window[1]:
            self.trend = 1
            psar = min(self.low_price_window)
            self.extreme_point = max(self.high_price_window)
        else:
            self.trend = 0
            psar = max(self.high_price_window)
            self.extreme_point = min(self.low_price_window)

        return psar

    def _calcPSAR(self):
        prev_psar = self.psar_list[-1]
        if self.trend == 1:  # Up
            psar = prev_psar + self.af * (self.extreme_point - prev_psar)
            psar = min(psar, min(self.low_price_window))
        else:
            psar = prev_psar - self.af * (prev_psar - self.extreme_point)
            psar = max(psar, max(self.high_price_window))

        return psar

    def _updateCurrentVals(self, psar, high, low):
        if self.trend == 1:
            self.high_price_trend.append(high)
        elif self.trend == 0:
            self.low_price_trend.append(low)

        psar = self._trendReversal(psar, high, low)

        self.psar_list.append(psar)
        self.af_list.append(self.af)
        self.ep_list.append(self.extreme_point)
        self.high_list.append(high)
        self.low_list.append(low)
        self.high_price_window.append(high)
        self.low_price_window.append(low)
        self.trend_list.append(self.trend)

        return psar

    def _trendReversal(self, psar, high, low):
        # Checks for reversals
        reversal = False
        if self.trend == 1 and psar > low:
            self.trend = 0
            psar = max(self.high_price_trend)
            self.extreme_point = low
            reversal = True
        elif self.trend == 0 and psar < high:
            self.trend = 1
            psar = min(self.low_price_trend)
            self.extreme_point = high
            reversal = True

        if reversal:
            self.af = self.init_af
            self.high_price_trend.clear()
            self.low_price_trend.clear()
        else:
            if high > self.extreme_point and self.trend == 1:
                self.af = min(self.af + self.af_step, self.max_af)
                self.extreme_point = high
            elif low < self.extreme_point and self.trend == 0:
                self.af = min(self.af + self.af_step, self.max_af)
                self.extreme_point = low

        return psar


def MADSignals(
    mad: pd.Series, long_level: float = 1.05, short_level: float = 0.95, shorts=SHORTS
) -> pd.Series:
    pos = np.zeros(len(mad))
    pos[:] = np.nan
    pos = np.where(mad > long_level, 1, pos)
    pos = np.where(mad < long_level, 0, pos)
    if shorts:
        pos = np.where(mad < short_level, -1, pos)
        pos = np.where((mad > short_level) & (mad < long_level), 0, pos)
    position = pd.Series(pos, index=mad.index).ffill().fillna(0)
    return position


def MADSigmaSignals(mad: pd.Series, sigma: pd.Series, shorts=SHORTS) -> pd.Series:
    pos = np.zeros(len(mad))
    pos[:] = np.nan
    pos = np.where(mad > 1 + sigma, 1, pos)
    if shorts:
        pos = np.where(mad < 1 - sigma, -1, pos)
        pos = np.where((mad > 1 - sigma) & (mad < 1 - sigma), 0, pos)
    position = pd.Series(pos, index=mad.index).ffill().fillna(0)
    return position


def MADStrategy(data):
    data["MAD"] = calcMAD(data["Close"])
    # data = data.dropna()
    data["position"] = MADSignals(data["MAD"], long_level=1.05)
    return calcReturns(data)


def MADSigmaStrategy(data):
    data["MAD"] = calcMAD(data["Close"])
    data["sigma"] = data["Close"].pct_change().rolling(21).std()
    # data = data.dropna()
    data["position"] = MADSigmaSignals(data["MAD"], data["sigma"])
    return calcReturns(data)


def RSIReversionStrategy(
    data, P=14, long_level=30, short_level=70, centerline=50, shorts=SHORTS
):
    """
    Goes long when RSI < long level, sells when the value crosses the
    centerline.
    Goes short when RSI > short level, covers when it crosses the
    centerline.
    """
    df = calcRSI(data, P=P)
    df["position"] = np.nan
    df["position"] = np.where(df["RSI"] < long_level, 1, df["position"])
    if shorts:
        df["position"] = np.where(df["RSI"] > short_level, -1, df["position"])
    if centerline is not None:
        # Exit when RSI crosses sell_level
        _sell_level = df["RSI"] - centerline
        df["cross_sell_level"] = _sell_level.shift(1) / _sell_level
        df["position"] = np.where(df["cross_sell_level"] < 0, 0, df["position"])
    else:
        df["position"] = np.where(df["RSI"] >= short_level, 0, df["position"])

    df["position"] = df["position"].ffill().fillna(0)

    return calcReturns(df)


def RSI2Strategy(
    data, P=2, SMA1=200, SMA2=5, long_level=10, short_level=90, shorts=SHORTS
):
    """
    Short-term RSI strategy based on 2-period RSI to find mean
    reversions within a larger trend.
    Long when price > SMA1 and RSI > long_level
        -> exit when price > SMA2
    Short when price < SMA1 and RSI < short_level
        -> exit when price < SMA2
    """
    df = calcRSI(data, P=P)
    df["SMA1"] = df["Close"].rolling(SMA1).mean()
    df["SMA2"] = df["Close"].rolling(SMA2).mean()
    position = np.zeros(df.shape[0])
    position[:] = np.nan
    # Enter long positions
    position = np.where(
        (df["RSI"] < long_level) & (df["Close"] > df["SMA1"]), 1, position
    )
    # Enter short positions
    if shorts:
        position = np.where(
            (df["RSI"] > short_level) & (df["Close"] < df["SMA1"]), -1, position
        )
    # Loop to add exits
    for i, _row in enumerate(df.iterrows()):
        row = _row[1]
        if np.isnan(row["SMA1"]):
            position[i] = 0
            continue
        # Exit positions based on SMA2, else hold
        if position[i - 1] == 1:
            if row["Close"] > row["SMA2"]:
                position[i] = 0
            else:
                position[i] = 1
        elif position[i - 1] == -1:
            if row["Close"] < row["SMA2"]:
                position[i] = 0
            else:
                position[i] = -1
    df["position"] = position
    df["position"] = df["position"].fillna(0)

    return calcReturns(df)


def RSIMomentumStrategy(data, P=14, centerline=50, upper=70, lower=30, shorts=SHORTS):
    """
    Buy when RSI crosses above the centerline, sell if it breaks above
    the upper threshold then drops below it again or if it goes below
    the centerline.
    Short if it drops below the centerline and cover if it breaks
    below the lower threshold and then above it, or above the
    centerline.
    """
    df = calcRSI(data, P=P)
    position = np.zeros(df.shape[0])
    for i, _row in enumerate(df.iterrows()):
        row = _row[1]
        if np.isnan(row["RSI"]):
            last_row = row.copy()
            continue
        if row["RSI"] > centerline and last_row["RSI"] < centerline:
            # Buy if no position
            if position[i - 1] != 1:
                position[i] = 1
        elif row["RSI"] > centerline and position[i - 1] == 1:
            # Check if value has retraced
            if last_row["RSI"] > upper and row["RSI"] < upper:
                position[i] = 0
            else:
                position[i] = 1
        elif position[i - 1] == 1 and row["RSI"] < centerline:
            if shorts:
                position[i] = 0
            else:
                position[i] = -1
        elif shorts:
            if row["RSI"] < centerline and last_row["RSI"] > centerline:
                # Short if no position
                if position[i - 1] != -1:
                    position[i] = -1
                elif row["RSI"] < centerline and position[i - 1] == -1:
                    # Check if value has retraced
                    if last_row["RSI"] < lower and row["RSI"] > lower:
                        position[i] = 0
                    else:
                        position[i] = -1
                elif position[i - 1] == -1 and row["RSI"] > centerline:
                    position[i] = 1
        last_row = row.copy()
    df["position"] = position
    return calcReturns(df)


def RSITrendStrategy(data, P=14, sma1=5, sma2=20, centerline=50, shorts=SHORTS):
    """
    Buy when RSI crosses above centerline and when SMA1>SMA2.
    Sell if one of the conditions are no longer met.
    Short if RSI is below centerline and SMA1<SMA2
    """
    df = calcRSI(data, P=P)
    df["SMA1"] = df["Close"].rolling(sma1).mean()
    df["SMA2"] = df["Close"].rolling(sma2).mean()
    df["position"] = np.nan
    df["position"] = np.where(
        (df["RSI"] > centerline) & (df["SMA1"] > df["SMA2"]), 1, df["position"]
    )
    df["position"] = np.where(
        (df["RSI"] < centerline) & (df["SMA1"] > df["SMA2"]), 0, df["position"]
    )
    df["position"] = np.where(
        (df["RSI"] > centerline) & (df["SMA1"] < df["SMA2"]), 0, df["position"]
    )

    if shorts:
        df["position"] = np.where(
            (df["RSI"] > centerline) & (df["SMA1"] > df["SMA2"]), -1, df["position"]
        )
    else:
        df["position"] = np.where(
            (df["RSI"] > centerline) & (df["SMA1"] > df["SMA2"]), 0, df["position"]
        )
    df["position"] = df["position"].ffill().fillna(0)
    return calcReturns(df)


def midDonCrossOver(data: pd.DataFrame, period: int = 20, shorts: bool = True):
    data = calcDonchianChannels(data, period)

    data["position"] = np.nan
    data["position"] = np.where(data["Close"] > data["midDon"], 1, data["position"])
    if shorts:
        data["position"] = np.where(
            data["Close"] < data["midDon"], -1, data["position"]
        )
    else:
        data["position"] = np.where(data["Close"] < data["midDon"], 0, data["position"])
    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def donChannelBreakout(data, period=20, shorts=SHORTS):
    data = calcDonchianChannels(data, period)

    data["position"] = np.nan
    data["position"] = np.where(
        data["Close"] > data["upperDon"].shift(1), 1, data["position"]
    )
    if shorts:
        data["position"] = np.where(
            data["Close"] < data["lowerDon"].shift(1), -1, data["position"]
        )
    else:
        data["position"] = np.where(
            data["Close"] < data["lowerDon"].shift(1), 0, data["position"]
        )

    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def donReversal(data, period=20, shorts=SHORTS):
    data = calcDonchianChannels(data, period)

    data["position"] = np.nan
    data["position"] = np.where(
        data["Close"] < data["lowerDon"].shift(1), 1, data["position"]
    )

    short_val = -1 if shorts else 0
    data["position"] = np.where(
        data["Close"] > data["lowerDon"].shift(1), short_val, data["position"]
    )

    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def PSARTrendStrategy(
    data: pd.DataFrame,
    init_af: float = 0.02,
    max_af: float = 0.2,
    af_step: float = 0.02,
    shorts: bool = True,
):
    indic = PSAR(init_af, max_af, af_step)
    data["PSAR"] = data.apply(lambda x: indic.calcPSAR(x["High"], x["Low"]), axis=1)
    data["Trend"] = indic.trend_list

    # Long when trend is up, short when trend is down
    data["position"] = 0
    data["position"] = np.where(data["Trend"] == 1, 1, data["position"])
    if shorts:
        data["position"] = np.where(data["Trend"] == 0, -1, data["position"])
    return calcReturns(data)


def RSITrendPSARStrategy(
    data: pd.DataFrame,
    init_af: float = 0.02,
    max_af: float = 0.2,
    af_step: float = 0.02,
    rsi_p: int = 7,
    centerline: float = 50,
    shorts: bool = True,
):
    """
    Enter when RSI moves above/below centerline and PSAR is in agreement with the
    move. Exit when either reverses.
    """
    # Calculate indicators
    data = calcRSI(data, rsi_p)
    indic = PSAR(init_af, max_af, af_step)
    data["PSAR"] = data.apply(lambda x: indic.calcPSAR(x["High"], x["Low"]), axis=1)
    data["Trend"] = indic.trend_list

    data["position"] = np.nan
    data["position"] = np.where(
        (data["RSI"] > centerline) & (data["Trend"] == 1), 1, data["position"]
    )
    if shorts:
        data["position"] = np.where(
            (data["RSI"] < centerline) & (data["Trend"] == 0), -1, data["position"]
        )

    data["position"] = data["position"].fillna(0)
    return calcReturns(data)


def TrendIntensityPSARStrategy(
    data: pd.DataFrame,
    init_af: float = 0.02,
    max_af: float = 0.2,
    af_step: float = 0.02,
    tii_p: int = 30,
    centerline: float = 50,
    shorts: bool = True,
):
    """
    Long if PSAR is in up trend and TII > centerline.
    Short/sells if PSAR is in down trend and TII < centerline.
    """
    # Calculate indicators
    data = calcTrendIntensityIndex(data, tii_p)
    indic = PSAR(init_af, max_af, af_step)
    data["PSAR"] = data.apply(lambda x: indic.calcPSAR(x["High"], x["Low"]), axis=1)
    data["Trend"] = indic.trend_list

    data["position"] = np.nan
    data["position"] = np.where(
        (data["TII"] > centerline) & (data["Trend"] == 1), 1, data["position"]
    )
    if shorts:
        data["position"] = np.where(
            (data["TII"] < centerline) & (data["Trend"] == 0), -1, data["position"]
        )

    data["position"] = data["position"].fillna(0)

    return calcReturns(data)


def BBMeanReversion(data, periods=20, m=2, shorts=SHORTS):
    """
    Buy/short when price moves outside of bands.
    Exit position when price crosses SMA(TP).
    """
    data = calcBollingerBand(data, periods, m)
    # Get points where price crosses SMA(TP)
    xs = (data["Close"] - data["TP_SMA"]) / (
        data["Close"].shift(1) - data["TP_SMA"].shift(1)
    )
    data["position"] = np.nan
    data["position"] = np.where(data["Close"] <= data["LBB"], 1, data["position"])
    if shorts:
        data["position"] = np.where(data["Close"] >= data["UBB"], -1, data["position"])
    else:
        data["position"] = np.where(data["Close"] >= data["UBB"], 0, data["position"])
    # Exit when price crosses SMA(TP)
    data["position"] = np.where(np.sign(xs) == -1, 0, data["position"])
    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def BBBreakout(data, periods=20, m=1, shorts=SHORTS):
    """
    Buy/short when price moves outside of the upper band.
    Exit when the price moves into the band.
    """
    data = calcBollingerBand(data, periods, m)
    data["position"] = np.nan
    data["position"] = np.where(data["Close"] > data["UBB"], 1, 0)
    if shorts:
        data["position"] = np.where(data["Close"] < data["LBB"], -1, data["position"])
    data["position"] = data["position"].ffill().fillna(0)
    return calcReturns(data)


def DoubleBBBreakout(data, periods=20, m1=1, m2=2, shorts=SHORTS):
    """
    Buy/short when price moves outside of the inner band (m1).
    Exit when the price moves into the inner band or to the outer
    bound (m2).
    """
    assert m2 > m1, f"m2 must be greater than m1:\nm1={m1}\tm2={m2}"
    data = calcBollingerBand(data, periods, m1, label="m1")
    data = calcBollingerBand(data, periods, m2, label="m2")
    data["position"] = np.nan
    data["position"] = np.where(data["Close"] > data["UBB_m1"], 1, 0)
    if shorts:
        data["position"] = np.where(
            data["Close"] < data["LBB_m1"], -1, data["position"]
        )
    data["position"] = np.where(data["Close"] > data["UBB_m2"], 0, data["position"])
    data["position"] = np.where(data["Close"] < data["LBB_m2"], 0, data["position"])
    data["position"] = data["position"].ffill().fillna(0)
    return calcReturns(data)


def BBWidthEMACross(data, periods=20, m=2, N=20, EMA1=10, EMA2=30, shorts=SHORTS):
    """
    Buys when Band Width reaches 20-day low and EMA1 > EMA2.
    Shorts when Band Width reaches 20-day low and EMA1 < EMA2.
    Exits position when EMA reverses.
    :periods: number of periods for Bollinger Band calculation.
    :m: number of standard deviations for Bollinger Band.
    :N: number of periods used to find a low.
    :EMA1: number of periods used in the short-term EMA signal.
    :EMA2: number of periods used in the long-term EMA signal.
    :shorts: boolean value to indicate whether or not shorts are
        allowed.
    """
    assert EMA1 < EMA2, f"EMA1 must be less than EMA2."
    # Calculate indicators
    data = calcBollingerBand(data, periods, m)
    data["width"] = (data["UBB"] - data["LBB"]) / data["TP_SMA"]
    data["min_width"] = data["width"].rolling(N).min()
    data = calcEMA(data, EMA1)
    data = calcEMA(data, EMA2)
    data["position"] = np.nan
    data["position"] = np.where(
        (data["width"] == data["min_width"])
        & (data[f"EMA_{EMA1}"] > data[f"EMA_{EMA2}"]),
        1,
        0,
    )
    if shorts:
        data["position"] = np.where(
            (data["width"] == data["min_width"])
            & (data[f"EMA_{EMA1}"] < data[f"EMA_{EMA2}"]),
            -1,
            data["position"],
        )
    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def StochRSIReversionStrategy(
    data, P=14, N=14, short_level=80, buy_level=20, shorts=SHORTS
):
    """
    Buys when the StochRSI is oversold and sells when it's
    overbought
    """
    df = calcStochRSI(data, P, N)
    df["position"] = np.nan
    df["position"] = np.where(df["StochRSI"] < buy_level, 1, df["position"])
    if shorts:
        df["position"] = np.where(df["StochRSI"] > short_level, -1, df["position"])
    else:
        df["position"] = np.where(df["StochRSI"] > short_level, 0, df["position"])

    df["position"] = df["position"].ffill().fillna(0)
    return calcReturns(df)


def StochRSIMomentumStrategy(data, P=14, N=14, centerline=50, shorts=SHORTS):
    """
    Buys when the StochRSI moves above the centerline,
    sells when it moves below
    """
    df = calcStochRSI(data, P, N)
    df["position"] = np.nan
    df["position"] = np.where(df["StochRSI"] > 50, 1, df["position"])
    if shorts:
        df["position"] = np.where(df["StochRSI"] < 50, -1, df["position"])
    else:
        df["position"] = np.where(df["StochRSI"] < 50, 0, df["position"])
    df["position"] = df["position"].ffill().fillna(0)

    return calcReturns(df)


def MACDReversionStrategy(data, N_fast=12, N_slow=26, level=1, shorts=SHORTS):
    df = calcMACD(data, N_fast, N_slow)
    df["position"] = np.nan
    df["position"] = np.where(df["MACD"] < level, 1, df["position"])
    if shorts:
        df["position"] = np.where(df["MACD"] > level, -1, df["position"])

    df["position"] = np.where(df["MACD"].shift(1) / df["MACD"] < 0, 0, df["position"])
    df["position"] = df["position"].ffill().fillna(0)

    return calcReturns(df)


def MACDSignalStrategy(data, N_fast=12, N_slow=26, N_sl=9, shorts=SHORTS):
    df = calcMACDSignal(data, N_fast, N_slow, N_sl)
    df["position"] = np.nan
    df["position"] = np.where(df["MACD"] > df[f"SignalLine_{N_sl}"], 1, df["position"])

    if shorts:
        df["position"] = np.where(
            df["MACD"] < df[f"SignalLine_{N_sl}"], -1, df["position"]
        )
    else:
        df["position"] = np.where(
            df["MACD"] < df[f"SignalLine_{N_sl}"], 0, df["position"]
        )
    df["position"] = df["position"].ffill().fillna(0)
    return calcReturns(df)


def MACDMomentumStrategy(data, N_fast=12, N_slow=26, N_sl=9, N_mom=3, shorts=SHORTS):
    df = calcMACDBars(data, N_fast=N_fast, N_slow=N_slow, N_sl=N_sl)
    df["growth"] = np.sign(df["MACD_bars"].diff(1))
    df["momentum"] = df["growth"].rolling(N_mom).sum()
    if shorts:
        df["position"] = df["momentum"].map(
            lambda x: np.sign(x) * 1 if np.abs(x) == N_mom else 0
        )
    else:
        df["position"] = df["momentum"].map(lambda x: 1 if x == N_mom else 0)
    df["position"] = df["position"].ffill().fillna(0)
    return calcReturns(df)


def MACDSignalMomentumStrategy(
    data, N_fast=12, N_slow=26, N_sl=9, N_mom=3, shorts=SHORTS
):
    df = calcMACDBars(data, N_fast=N_fast, N_slow=N_slow, N_sl=N_sl)
    df["growth"] = np.sign(df["MACD_bars"].diff(1))
    df["momentum"] = df["growth"].rolling(N_mom).sum()
    # Enter a long/short position if momentum is going in the right
    # direction and wait for cross-over
    position = np.zeros(len(df)) + np.nan
    for i, _row in enumerate(data.iterrows()):
        row = _row[1]
        mom = row["momentum"]
        if np.isnan(mom):
            last_row = row.copy()
            continue
        if np.abs(mom) == N_mom and position[i - 1] == 0:
            # Enter new position
            if shorts:
                position[i] = np.sign(mom)
            else:
                position[i] = 1 if np.sign(mom) == 1 else 0
        elif row["MACD_bars"] / last_row["MACD_bars"] < 0:
            # Change in sign indicates cross-over -> exit
            position[i] = 0
        else:
            # Hold position
            position[i] = position[i - 1]

    df["position"] = position

    return calcReturns(df)


def RSIDivergenceStrategy_(data, P=14, order=5, K=2):
    """
    Go long/short on price and RSI divergence.
    - Long if price to lower low and RSI to higher low with RSI < 50
    - Short if price to higher high and RSI to lower high with RSI > 50
    Sell if divergence disappears.
    Sell if the RSI crosses the centerline.
    """
    data = getPeaks(data, key="Close", order=order, K=K)
    data = calcRSI(data, P=P)
    data = getPeaks(data, key="RSI", order=order, K=K)

    position = np.zeros(data.shape[0])
    for i, (t, row) in enumerate(data.iterrows()):
        if np.isnan(row["RSI"]):
            continue
        # If no position is on
        if position[i - 1] == 0:
            # Buy if indicator to higher low and price to lower low
            if row["Close_lows"] == -1 and row["RSI_lows"] == 1:
                if row["RSI"] < 50:
                    position[i] = 1
                    entry_rsi = row["RSI"].copy()

            # Short if price to higher high and indicator to lower high
            elif row["Close_highs"] == 1 and row["RSI_highs"] == -1:
                if row["RSI"] > 50:
                    position[i] = -1
                    entry_rsi = row["RSI"].copy()

        # If current position is long
        elif position[i - 1] == 1:
            if row["RSI"] < 50 and row["RSI"] < entry_rsi:
                position[i] = 1

        # If current position is short
        elif position[i - 1] == -1:
            if row["RSI"] < 50 and row["RSI"] > entry_rsi:
                position[i] = -1

    data["position"] = position
    return calcReturns(data)


def RSIDivergenceStrategy(data, P=14, order=5, K=2, shorts=SHORTS):
    """
    Go long/short on price and RSI divergence.
    - Long if price to lower low and RSI to higher low with RSI < 50
    - Short if price to higher high and RSI to lower high with RSI > 50
    Sell if divergence disappears.
    Sell if the RSI crosses the centerline.
    """
    data = getPeaks(data, key="Close", order=order, K=K)
    data = calcRSI(data, P=P)
    data = getPeaks(data, key="RSI", order=order, K=K)

    position = np.zeros(data.shape[0])
    for i, (t, row) in enumerate(data.iterrows()):
        if np.isnan(row["RSI"]):
            continue
        # If no position is on
        if position[i - 1] == 0:
            # Buy if indicator to higher low and price to lower low
            if row["Close_lows"] == -1 and row["RSI_lows"] == 1:
                if row["RSI"] < 50:
                    position[i] = 1
                    entry_rsi = row["RSI"]

            # Short if price to higher high and indicator to lower high
            elif row["Close_highs"] == 1 and row["RSI_highs"] == -1:
                if row["RSI"] > 50:
                    position[i] = -1 if shorts else 0
                    entry_rsi = row["RSI"]

        # If current position is long
        elif position[i - 1] == 1:
            if row["RSI"] < 50 and row["RSI"] < entry_rsi:
                position[i] = 1

        # If current position is short
        elif position[i - 1] == -1:
            if row["RSI"] < 50 and row["RSI"] > entry_rsi:
                position[i] = -1

    data["position"] = position
    return calcReturns(data)


def RSIDivergenceWithTrendStrategy_(data, P=14, order=5, K=2, EMA1=50, EMA2=200):
    """
    Go long/short on price and RSI divergence.
    - Long if price to lower low and RSI to higher low with RSI < 50
    - Short if price to higher high and RSI to lower high with RSI > 50
    Sell if divergence disappears or if the RSI crosses the centerline, unless
    there is a trend in the same direction.
    """
    data = getPeaks(data, key="Close", order=order, K=K)
    data = calcRSI(data, P=P)
    data = getPeaks(data, key="RSI", order=order, K=K)
    data = calcEMA(data, EMA1)
    data = calcEMA(data, EMA2)
    position = np.zeros(data.shape[0])

    for i, (t, row) in enumerate(data.iterrows()):
        if np.isnan(row["RSI"]):
            continue
        # If no position is on
        if position[i - 1] == 0:
            # Buy if indicator to higher high and price to lower high
            if row["Close_lows"] == -1 and row["RSI_lows"] == 1:
                if row["RSI"] < 50:
                    position[i] = 1
                    entry_rsi = row["RSI"].copy()

            # Short if price to higher high and indicator to lower high
            elif row["Close_highs"] == 1 and row["RSI_highs"] == -1:
                if row["RSI"] > 50:
                    position[i] = -1
                    entry_rsi = row["RSI"].copy()

        # If current position is long
        elif position[i - 1] == 1:
            if row["RSI"] < 50 and row["RSI"] < entry_rsi:
                position[i] = 1
            elif row[f"EMA_{EMA1}"] > row[f"EMA_{EMA2}"]:
                position[i] = 1

        # If current position is short
        elif position[i - 1] == -1:
            if row["RSI"] < 50 and row["RSI"] > entry_rsi:
                position[i] = -1
            elif row[f"EMA_{EMA1}"] < row[f"EMA_{EMA2}"]:
                position[i] = -1

    data["position"] = position

    return calcReturns(data)


def RSIDivergenceWithTrendStrategy(
    data, P=14, order=5, K=2, EMA1=50, EMA2=200, shorts=SHORTS
):
    """
    Go long/short on price and RSI divergence.
    - Long if price to lower low and RSI to higher low with RSI < 50
    - Short if price to higher high and RSI to lower high with RSI > 50
    Sell if divergence disappears or if the RSI crosses the centerline, unless
    there is a trend in the same direction.
    """
    data = getPeaks(data, key="Close", order=order, K=K)
    data = calcRSI(data, P=P)
    data = getPeaks(data, key="RSI", order=order, K=K)
    data = calcEMA(data, EMA1)
    data = calcEMA(data, EMA2)
    position = np.zeros(data.shape[0])

    for i, (t, row) in enumerate(data.iterrows()):
        if np.isnan(row["RSI"]):
            continue
        # If no position is on
        if position[i - 1] == 0:
            # Buy if indicator to higher high and price to lower high
            if row["Close_lows"] == -1 and row["RSI_lows"] == 1:
                if row["RSI"] < 50:
                    position[i] = 1
                    entry_rsi = row["RSI"]

            # Short if price to higher high and indicator to lower high
            elif row["Close_highs"] == 1 and row["RSI_highs"] == -1:
                if row["RSI"] > 50:
                    position[i] = -1 if shorts else 0
                    entry_rsi = row["RSI"]

        # If current position is long
        elif position[i - 1] == 1:
            if row["RSI"] < 50 and row["RSI"] < entry_rsi:
                position[i] = 1
            elif row[f"EMA_{EMA1}"] > row[f"EMA_{EMA2}"]:
                position[i] = 1

        # If current position is short
        elif position[i - 1] == -1:
            if row["RSI"] < 50 and row["RSI"] > entry_rsi:
                position[i] = -1
            elif row[f"EMA_{EMA1}"] < row[f"EMA_{EMA2}"]:
                position[i] = -1

    data["position"] = position

    return calcReturns(data)


def SMAMeanReversion(data, sma=50, threshold=0.1, shorts=SHORTS):
    data["SMA"] = data["Close"].rolling(sma).mean()
    data["extension"] = (data["Close"] - data["SMA"]) / data["SMA"]

    data["position"] = np.nan
    data["position"] = np.where(data["extension"] < -threshold, 1, data["position"])
    if shorts:
        data["position"] = np.where(data["extension"] > threshold, -1, data["position"])

    data["position"] = np.where(np.abs(data["extension"]) < 0.01, 0, data["position"])
    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)


def SMAMeanReversionSafety(
    data, sma=50, threshold=0.1, safety_threshold=0.25, shorts=SHORTS
):
    data["SMA"] = data["Close"].rolling(sma).mean()
    data["extension"] = (data["Close"] - data["SMA"]) / data["SMA"]

    data["position"] = np.nan
    data["position"] = np.where(
        (data["extension"] < -threshold) & (data["extension"] > -safety_threshold),
        1,
        data["position"],
    )

    if shorts:
        data["position"] = np.where(
            (data["extension"] > threshold) & (data["extension"] < safety_threshold),
            -1,
            data["position"],
        )

    data["position"] = np.where(np.abs(data["extension"]) < 0.01, 0, data["position"])
    data["position"] = data["position"].ffill().fillna(0)

    return calcReturns(data)
