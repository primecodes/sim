from pathlib import Path

import matplotlib.pyplot as plt


def savefig(plt, fname):
    Path(fname).parent.mkdir(exist_ok=True, parents=True)
    plt.savefig(fname)


def plot(df, ticker, fname):
    try:
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        fig, ax = plt.subplots(1, figsize=(12, 4))
        ax.plot(df['Close'], color='k')
        ax.yaxis.tick_right()
        ax.set_ylabel('Prices')
        ax.set_title(f'{ticker} Prices')
        plt.tight_layout()
        savefig(plt, fname)
    finally:
        plt.close()


def plot_2(df, ticker, label, fname):
    try:
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        fig, ax = plt.subplots(2, figsize=(12, 6), sharex=True)
        ax[0].plot(df['Close'], color='k')
        ax[0].yaxis.tick_right()
        ax[0].set_ylabel('Prices')
        ax[0].set_title(f'{ticker} Prices')
        ax[1].plot(df['strat_cum_returns'] * 100, label=label, color='k')
        ax[1].plot(df['cum_returns'] * 100, label='Buy and Hold', color='#ccc')
        ax[1].yaxis.tick_right()
        ax[1].set_ylabel('Returns (%)')
        ax[1].set_title(
            f'Cumulative Returns for {label} and Buy and Hold Strategies for {ticker}'
        )
        ax[1].legend(loc='upper left', ncol=2, framealpha=0.25)
        plt.tight_layout()
        savefig(plt, fname)
    finally:
        plt.close()